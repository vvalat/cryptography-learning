"""
Display a view and count of the bits changed between two hashes supplied when invoking the script
"""
import sys


if __name__ == "__main__":
    args = sys.argv
    if len(args) != 3 or len(args[1]) != len(args[2]):
        print("Two inputs of same size are required")
        sys.exit(1)
    bit_one, bit_two = bin(int(args[1], 16))[2:], bin(int(args[2], 16))[2:]  # Lose the "0b" prefix
    diff_count = 0
    string_repr = ""
    # Some 0-padding
    if (bit_diff := len(bit_one) - len(bit_two)) != 0:
        bit_two = f"{'0' * bit_diff}{bit_two}"
        bit_one = f"{'0' * -bit_diff}{bit_one}"
    for (b1, b2) in zip(bit_one, bit_two):
        if b1 != b2:
            diff_count += 1
            string_repr += "X"
        else:
            string_repr += "_"
    print(f"{diff_count} differences over a {len(bit_one)} count\n{string_repr}")
