"""
Basic implementation of the Caesar's shifting cypher
Note: The book also recommends implementing a substitution scheme based on pseudo random elements
    with unique indexing. However, to do it properly would require using a data structure of 26!
    elements. Since the goal is to make clear that the higher the possibilities are, the harder
    brute-forcing will be (taking linguistic and statistical biases into account), writing code
    for it is neither hard nor interesting. For completion's sake, here's a possible solution that
    only generates ten random sets (collisions and duplicates sets are possible (though unlikely)
    and can generate less than ten results):
        output = {}
        for i in range(0, 10):
            alphabet = list(range(0, 26))
            random.shuffle(alphabet)
            substitutions = {}
            for index, position in enumerate(alphabet):
                substitutions[char(ord("A") + index] = char(ord("A") + position)
            output[some_hash_function(substitutions)] = substitutions
        ...
        new_char = output[some_value][old_char]
"""
import sys
import os
import re


def sanitize(input_string: str, remove_spaces: bool = True) -> str:
    """
    Returns a single-spaced uppercase string from any input
    :param input_string: The input sequence
    :param remove_spaces: Whether we remove spaces or not
    """
    separator = "" if remove_spaces else " "
    return re.sub(
        r"\s+",
        " ",
        "".join([letter.upper() if letter.isalpha() else separator for letter in input_string])
    )


def encrypt(text: str, shift: int) -> str:
    """
    Returns an encrypted string using the Caesar's cypher
    Rationale:
        new char = chr(ord("A") + ((ord(char) - ord("A") + shift) % 26))
                        start         current position     shift with overflow
    :param text: The input sequence
    :param shift: How many letters in the alphabet should the cypher use for shifting (default: 3)
    """
    return "".join([
        chr(ord("A") + ((ord(char) - ord("A") + (shift or 3)) % 26)) if char != " " else " "
        for char in text
    ])


def decrypt(text: str, shift: int) -> str:
    """
    Inversion of the above method
    """
    return "".join([chr(ord("A") + ((ord(char) - ord("A") - shift) % 26)) for char in text])


def break_code(text: str):
    data_dict = {}
    with open(os.path.join(os.path.curdir, "resources", "most_common_english_words")) as file:
        words = file.read().split("\n")
    for shift in range(1, 27):
        count = 0
        candidate = decrypt(text, shift)
        for word in words:
            if word.upper() in candidate:
                count += 1
        data_dict[shift] = count
    prob_shift = max(data_dict, key=data_dict.get)
    print(f"Probable shift value: {prob_shift} with decoded message: {decrypt(text, prob_shift)}")


if __name__ == "__main__":
    args = sys.argv
    if (valid_shift := (len(args) == 3 and args[2].isnumeric())) or len(args) == 2:
        plaintext = args[1]
        cyphertext = encrypt(sanitize(plaintext), int(args[2]) if valid_shift else None)
        print("Encrypted message: ", cyphertext)
        break_code(cyphertext)
    else:
        print("Use a quoted string as first input and optionally a number for shifting")
        sys.exit(1)
